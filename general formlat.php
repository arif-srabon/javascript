<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            {!! Form::label('client_id', "Center Registration / Pin No. / Client ID No. *") !!}
            <div class="input-group">
                {!! Form::text('client_id', null, ['class' => 'form-control', 'placeholder' => 'Center Registration / Pin No. /  Client ID No. *']) !!}
                <span class="input-group-btn">
                                 <button type="button" class="btn bg-primary"><i class="icon-search4 position-left"></i>
                                     Find
                                 </button>
                            </span>
            </div>
            @if ($errors->has('client_id'))
                <p class="text-danger">{!!$errors->first('client_id')!!}</p>
            @endif
        </div>
    </div>
</div>


<div class="tabbable">
    <ul class="nav nav-tabs nav-tabs-highlight">
        <li class="active">
            <a href="#left-icon-tab1" data-toggle="tab"><i class="icon-menu7 position-left"></i>
                Prescription</a>
        </li>
        <li>
            <a href="#left-icon-tab2" data-toggle="tab"><i class="icon-mention position-left"></i> Inactive</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-3">

        </div>
        <div class="col-md-9">
            <div class="tab-content">
                <div class="tab-pane active" id="left-icon-tab1">
                    <div class="row" style="border: 1px solid #CCCCCC;">
                        <div class=" col-md-12">
                            <legend class="text-semibold">Prescription</legend>
                            <div class="form-group">
                                {!! Form::label('chief_complain', 'Chief Complain (C/C)') !!}
                                {!! Form::textarea('chief_complain', null, ['class' => 'form-control', 'placeholder' => 'Chief Complain', 'rows'=>'3']) !!}
                                @if ($errors->has('chief_complain'))<p
                                        class="text-danger">{!!$errors->first('chief_complain')!!}</p>@endif
                            </div>

                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="left-icon-tab2">
                    <div class="row" style="border: 1px solid #CCCCCC;">
                        <div class="col-md-12">
                            <legend class="text-semibold">Details Prescription</legend>
                            <div>
                                <label><strong>Chief Complain (C/C): </strong></label>

                                <p style="border: 1px solid #ccc;padding-left: 20px;max-height: 100px;overflow: auto;background: #ECF4FC">
                                    bootsnipp.com/tags/profile
                                    ... developers. Find snippets using HTML, CSS, Javascript, jQuery, Bootstrap, and
                                    more. ... User Profile Sidebar � User Profile Sidebar. 72 1 � User Profile Widget.
                                    ?
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="text-right">
    {!! Form::hidden('id') !!}
    <button id="reset" class="btn btn-default" type="reset">Reset
        <i class="icon-reload-alt position-right"></i>
    </button>
    <button class="btn btn-primary" type="submit">Save <i class="icon-arrow-right14 position-right"></i></button>
</div>