<html>
  
   <p>
    <?php
    // Get the length of your own name
    // and print it to the screen!
    $myname="Md. Ariful Islam Srabon";
    $length=strlen($myname);
    echo $length;
    ?>
  </p>
  <p>
    <?php
    // Get a partial string from within your own name
    // and print it to the screen!
    $myname="Ariful Islam Srabon";
    $partial=substr($myname,13,6);
    echo $partial;

    ?>
  </p>
  <p>
    <?php
    // Make your name upper case and print it to the screen:
    $uppercases=strtoupper($myname);
    echo $uppercases;
    ?>
  </p>
  <p>
    <?php
    // Make your name lower case and print it to the screen:
    $lowercase=strtolower($myname);
    echo $lowercase;
    ?>
  </p>
  
  <p>
    <?php
    // Print out the position of a letter that is in
    // your own name
    $myvalue="Ariful Islam Srabon";
    $string="u";
    $pos=strpos($myvalue,$string);
    var_dump( $pos);
    ?>
    </p>
    <p>
    <?php
    // Check for a false value of a letter that is not
    // in your own name and print out an error message
    if($pos===false){
     echo "The string '$string' was not found in the string '$myvalue'";
    }else{
       echo "The string '$string' was found in the string '$myvalue'";
    echo " and exists at position $pos"; 
        }
    ?>
    </p>
	
	*************************** MATH FUNCTION ****************************
	
	   <p>
    <?php
    // Try rounding a floating point number to an integer
    // and print it to the screen
    $round=round(M_PI);
    echo $round;
    ?>
    </p>
    <p>
    <?php
    // Try rounding a floating point number to 3 decimal places
    // and print it to the screen
    $round_decimal=round(M_PI,4);
    echo $round_decimal;
    ?>
    </p>
	
	<p>
    <?php
    // Use rand() to print a random number to the screen

    print rand(100,500);
   
    ?>
    </p>
    <p>
    <?php
    // Use your knowledge of strlen(), substr(), and rand() to
    // print a random character from your name to the screen.
     $name="arifulislamsrabon";
     $max=strlen($name);
     $lastPosition=$max-1;
     echo $lastPosition."<br>";
     for($i=0;$i<=100;$i++){
     $count=rand(0,$lastPosition);
     $char=substr($name,$count,1);
     echo $count." "."|". $char."<br>";
     }
    ?>
    </p>
	************************ ARRAY FUNCTION PART I *********************
	   <p>
	<?php
	// Create an array and push 5 elements on to it, then 
    // print the number of elements in your array to the screen
    $array=array();
    array_push($array,"Black");
    array_push($array,"Green");
    array_push($array,"Red");
    array_push($array,"Yellow");
    array_push($array,"Blue");
    echo "<pre>";
    print_r($array);
    echo "</pre>";
	$count=count($array);
    echo $count;
	?>
	</p>
	
	    <p>
	<?php
	// Create an array with several elements in it,
	// then sort it and print the joined elements to the screen
    $the_array=array('4','2','3','1','5');
    sort($the_array);
    print join(",",$the_array);
	?>
	</p>
	<p>
	<?php
	// Reverse sort your array and print the joined elements to the screen
	rsort($the_array);
	print join(",",$the_array);

	?>
	</p>
	
	  <p>
	<?php
	// Create an array and push on the names
    // of your closest family and friends
    $the_array=array();
    array_push($the_array,"Arif");
    array_push($the_array,"Srabon");
    array_push($the_array,"Ariyan");
    array_push($the_array,"Ayrin");
    array_push($the_array,"Digontu");
	// Sort the list
    sort($the_array);
	// Randomly select a winner!
    $count=count($the_array);
    $num=rand(0,$count);
    $winner=$the_array[$num];
	// Print the winner's name in ALL CAPS
	echo strtoupper($winner);
	?>
	</p>
	
	************************ ARRAY FUNCTION PART II *********************
	<p>
        <?php
          $name="Ariful Islam Srabon";
          $len=strlen($name);
          echo $len;
        ?>
      </p>
	  
	  <p>
        <?php
          // Here we define the function...
          function helloWorld() {
            echo "Hello world!";
          }
          
          // ...and here we call it!
          helloWorld();
        ?>
      </p>
	  
	  <p> <?php
        function returnName(){
            return $name="Ariful Islam Srabon";
            }
        $myName=returnName();
        print $myName;
      ?>
      </p>
	  
	  <p>
        <?php
        function greetings($name){
            echo "Greetings, " . $name . "!";
            }
            
            greetings("Ariful Islam Srabon");
        ?>
      </p>
	  
	   <p>
        <?php
      function aboutMe($name,$age){
          echo "Hello ! My Name is $name, and I am $age years old.";
          }
          aboutMe("Ariful Islam Srabon","25");
        ?>
      </p>
	  
	  **************** Objects of PHP ***************
	  
	<!DOCTYPE html>
<html>
    <head>
      <title> Introduction to Object-Oriented Programming </title>
      <link type='text/css' rel='stylesheet' href='style.css'/>
    </head>
	<body>
      <p>
      <?php
        // The code below creates the class
        class Person {
            // Creating some properties (variables tied to an object)
            public $isAlive = true;
            public $firstname;
            public $lastname;
            public $age;
            
            // Assigning the values
            public function __construct($firstname, $lastname, $age) {
              $this->firstname = $firstname;
              $this->lastname = $lastname;
              $this->age = $age;
            }
            
            // Creating a method (function tied to an object)
            public function greet() {
              return "Hello, my name is " . $this->firstname . " " . $this->lastname . ". Nice to meet you! :-)";
            }
          }
          
        // Creating a new person called "boring 12345", who is 12345 years old ;-)
        $me = new Person('boring', '12345', 12345);
        
        // Printing out, what the greet method returns
        echo $me->greet(); 
        ?>
        </p>
    </body>
</html>

********************end of object oriented programming***********************
</html>