<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('idsc_center_id', "Center Registration / Pin No. / Client ID No. *") !!}
            <div class="input-group">
                {!! Form::text('idsc_center_id', null, ['class' => 'form-control', 'placeholder' => 'Center Registration / Pin No. /  Client ID No. *']) !!}
                <span class="input-group-btn">
                                 <button type="button" class="btn bg-primary"><i class="icon-search4 position-left"></i>
                                     Find
                                 </button>
                            </span>
            </div>
            @if ($errors->has('client_id'))
                <p class="text-danger">{!!$errors->first('client_id')!!}</p>
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('assessment_id', 'Date of Assessment *') !!}
            <div class="input-group">
                {!! Form::date('assessment_id',new DateTime(), ['class' => 'form-control daterange-single daterange-left']) !!}
                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
            </div>

        </div>
    </div>
</div>

<div class="tabbable">
    <ul class="nav nav-tabs nav-tabs-highlight">
        <li class="active">
            <a href="#left-icon-tab1" data-toggle="tab"><i class="icon-menu7 position-left"></i>
                Eye Assessment</a>
        </li>
        <li>
            <a href="#left-icon-tab2" data-toggle="tab"><i class="icon-mention position-left"></i> Remarks</a>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-4">
            <legend class="text-semibold">
                <i class="icon-user position-left"></i>
                Client Information
            </legend>
        </div>
        <div class="col-md-8">
            <div class="tab-content">
                <div class="tab-pane active" id="left-icon-tab1">
                    <div class="row" style="border: 1px solid #CCCCCC;">
                        <div class=" col-md-12">
                            <legend class="text-semibold">Eye Assessment Information</legend>
                            <div class="col-md-12">
                            <div class="form-group">
                                <div class="panel-group panel-group-control content-group-rg" id="accordion-control">
                                    <div class="panel panel-white">
                                        <div class="panel-heading">
                                            <h6 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion-control" href="#accordion-control-group-right">
                                                    Right Eye Information
                                                </a>
                                            </h6>
                                        </div>
                                        <div id="accordion-control-group-right" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <table class="table table-bordered" border="1" text-align="center"
                                                           input="circle">
                                                        <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>SPHERE</th>
                                                            <th>CYLINDER</th>
                                                            <th>AXIS</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>D.V</td>
                                                            <td><input type="text" name="right_sphere_dv" class="form-control"></td>
                                                            <td><input type="text" name="right_cylinder_dv" class="form-control"></td>
                                                            <td><input type="text" name="right_axis_dv" class="form-control"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>N.V</td>
                                                            <td><input type="text" name="right_sphere_nv" class="form-control"></td>
                                                            <td><input type="text" name="right_cylinder_nv" class="form-control"></td>
                                                            <td><input type="text" name="right_axis_nv" class="form-control"></td>
                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-12">
                                <div class="form-group">

                                    <div class="panel-group panel-group-control content-group-lg" id="accordion-control">
                                        <div class="panel panel-white">
                                            <div class="panel-heading">
                                                <h6 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion-control" href="#accordion-control-group-left-eye">
                                                        Left Eye Information
                                                    </a>
                                                </h6>
                                            </div>
                                            <div id="accordion-control-group-left-eye" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <table class="table table-bordered" border="1" text-align="center">
                                                            <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>SPHERE</th>
                                                                <th>CYLINDER</th>
                                                                <th>AXIS</th>
                                                                <th>VISION</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>D.V</td>
                                                                <td><input type="text" name="left_sphere_dv" class="form-control"></td>
                                                                <td><input type="text" name="left_cylinder_dv" class="form-control"></td>
                                                                <td><input type="text" name="left_axis_dv" class="form-control"></td>
                                                                <td><input type="text" name="vision_dv" class="form-control"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>N.V</td>
                                                                <td><input type="text" name="left_sphere_nv" class="form-control"></td>
                                                                <td><input type="text" name="left_cylinder_nv" class="form-control"></td>
                                                                <td><input type="text" name="left_axis_nv" class="form-control"></td>
                                                                <td><input type="text" name="vision_nv" class="form-control"></td>
                                                            </tr>

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div calss="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('var', 'V.A.R.') !!}
                                        {!! Form::textarea('var', null, ['class' => 'form-control', 'placeholder' => 'V.A.R.', 'rows'=>'1']) !!}
                                        @if ($errors->has('var'))<p class="text-danger">{!!$errors->first('var')!!}</p>@endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('val', 'V.A.L.') !!}
                                        {!! Form::textarea('val', null, ['class' => 'form-control', 'placeholder' => 'V.A.L', 'rows'=>'1']) !!}
                                        @if ($errors->has('val'))<p class="text-danger">{!!$errors->first('val')!!}</p>@endif
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('ipd', 'I.P.D') !!}
                                        {!! Form::textarea('ipd', null, ['class' => 'form-control', 'placeholder' => 'I.P.D', 'rows'=>'1']) !!}
                                        @if ($errors->has('ipd'))<p class="text-danger">{!!$errors->first('ipd')!!}</p>@endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('remarks', 'Remarks.') !!}
                                    {!! Form::textarea('remarks', null, ['class' => 'form-control', 'placeholder' => '', 'rows'=>'4']) !!}
                                    @if ($errors->has('remarks'))<p class="text-danger">{!!$errors->first('remarks')!!}</p>@endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="left-icon-tab2">
                    <div class="row" style="border: 1px solid #CCCCCC;">
                        <div class="col-md-12">
                            <legend class="text-semibold">Eye Assessment Information</legend>





                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

</div>

<div class="row">
        <div class="text-right">
            {!! Form::hidden('id') !!}
            <button id="reset" class="btn btn-primary" type="reset">Reset
                <i class="icon-reload-alt position-right"></i>
            </button>
            <button class="btn btn-primary" type="submit">
                Submit form
                <i class="icon-arrow-right14 position-right"></i>
            </button>
        </div>
</div>