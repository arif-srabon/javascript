<?php
/**
 * Created by PhpStorm.
 * User: Arif
 * Date: 3/20/2016
 * Time: 4:06 PM
 */

namespace App\Http\Controllers\Jpuf;

use DB;
use App\Http\Controllers\Controller;
use App\Model\Setup\CommonConfigModel;
use mjanssen\BreadcrumbsBundle\Breadcrumbs;
use App\KendoModel as kds;
use Riesenia\Kendo\Kendo;

class OccupationalTherapyController extends Controller
{
    public $kds;

    public function __construct()
    {
        $this->kds = new kds;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        \Assets::add(['kendoui/kendo.common.min.css',
            'kendoui/kendo.default.min.css',
            'kendoui/kendo.all.min.js',

        ]);
        // breadcrumbs
        Breadcrumbs::addBreadcrumb('Assessment', '/occupationaltherapy');
        Breadcrumbs::addBreadcrumb('Occupational Therapy', '#');

        $transport_read_data = Kendo::createRead()
            ->setUrl('/occupationaltherapy/read')
            ->setContentType('application/json')
            ->setType('POST');

        $transport_data = Kendo::createTransport()
            ->setRead($transport_read_data)
            ->setParameterMap(Kendo::js('function(data) { return kendo.stringify(data); }'));

        $model_data = Kendo::createModel()
            ->setId('id');

        $schema_data = Kendo::createSchema()
            ->setData('data')
            ->setModel($model_data)
            ->setTotal('total');

        $dataSource_data = Kendo::createDataSource()
            ->setTransport($transport_data)
            ->setSchema($schema_data)
            ->setPageSize(config('app_config.num_paging_row'))
            ->setServerSorting(true)
            ->setServerPaging(true)
            ->setServerFiltering(true);

        $pageable = Kendo::createPageable();
        $pageable->setRefresh(true)
            ->setPageSizes(config('app_config.grid_page_size'))
            ->setButtonCount(5);

        $idsc_data = Kendo::createGrid('#grid_center')
            ->setDataSource($dataSource_data)
            ->setHeight(500)
            ->setScrollable(true)
            ->setSelectable('row')
            ->setSortable(true)
            ->setResizable(true)
            ->setFilterable(true)
            ->setPageable($pageable)
            ->setColumns([
                ['field' => 'assessment_date', 'title' => 'Date ', 'width' => "150px"],
                ['field' => 'client_pin_no', 'title' => 'Client ID No.'],
                ['field' => 'client_name', 'title' => 'Client Name (English)', 'width' => "200px"],
                ['field' => 'mobile', 'title' => 'Mobile'],
                ['field' => 'gender', 'title' => 'Gender'],


            ]);

        $command_print = ["click" => Kendo::js('commandPrint'), "text" => "Print"];
        $command_edit = ["click" => Kendo::js('commandEdit'), "text" => "Edit"];
        $command_del = ["click" => Kendo::js('commandDelete'), "text" => "Delete"];
        $command [] = $command_print;
        $command [] = $command_edit;
        $command [] = $command_del;

        $idsc_data->addColumns(null, ['command' => $command, 'title' => "&nbsp;", 'width' => "225px"]);

        $data = ['occupational_therapy' => $idsc_data];
        return view('jpuf.occupational_therapy.list', $data);
    }

    public function read()
    {


    }


    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        echo "I am Destroy Function()";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Assets::add(['plugins/forms/validation/validate.min.js',
            'plugins/forms/styling/uniform.min.js',
            'plugins/forms/selects/select2.min.js',
            'plugins/ui/moment/moment.min.js',
            'plugins/pickers/daterangepicker.js',
            'app/registration_form_validation.js'
        ]);

        // breadcrumbs
        Breadcrumbs::addBreadcrumb('Assessment', '/occupationaltherapy');
        Breadcrumbs::addBreadcrumb('Occupational Therapy', '/occupationaltherapy');
        Breadcrumbs::addBreadcrumb('Add', '#');

        $data = [];
        $data = $this->getOccupationalCommonData();

        return view('jpuf.occupational_therapy.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response ...
     */
    public function store()
    {
        echo "I am Store Function()";
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        echo "I am Show Function()";
    }

    public function edit()
    {
        echo "I am Edit Function()";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        echo "I am Update Function()";
    }



    public function getOccupationalCommonData()
    {
        $data=[];
        $cConfig = new CommonConfigModel;
        $data['deformities']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Deformities')));
        $data['musscletone']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Muscletone')));
        $data['sensation']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Sensatiion')));
        $data['hand']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Hand_Function')));
        $data['transitional']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Transitional_Movement_For_Child')));
        $data['grossmotor']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Functional_Gross_Motor_Skills')));
        $data['cognitive']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Cognitive_Skill')));
        $data['visual']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Viual_Processing_Skill_For_Adult')));
        $data['sensorymotor']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Sensory_Motor')));
        $data['finemotor']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Fine_Motor')));
        $data['forchild']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Only_For_Child')));
        $data['occupationalPerformance']=$cConfig->getOccupationalCommondataList($this->getmasterID(config('app_config.Occupational_Performance')));
        return $data;

    }
    public function getmasterID($masterCode){
        return DB::table('cc_speech_pediatric_type')
            ->where('is_active', 1)
            ->where('code','=',$masterCode)
            ->value('id');
    }


}

***********************************
CommonConfigModel:
 public function getOccupationalCommondataList($masterID)
    {
        return DB::table('cc_occupational_therapy')
            ->where('is_active', 1)
            ->where('type_id','=',$masterID)
            ->orderBy('weight', 'desc')
            ->orderBy('name', 'asc')
            ->get();
    }

****************************************
config file:
 /*
     * For Occupational Therapy
     */
    'Deformities'=>24,
    'Muscletone'=>25,
    'Sensatiion'=>26,
    'Hand_Function'=>27,
    'Transitional_Movement_For_Child'=>28,
    'Functional_Gross_Motor_Skills'=>29,
    'Cognitive_Skill'=>30,
    'Viual_Processing_Skill_For_Adult'=>31,
    'Sensory_Motor'=>32,
    'Fine_Motor'=>33,
    'Only_For_Child'=>34,
    'Occupational_Performance'=>35,