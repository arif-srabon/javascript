-- All - All

SELECT
COUNT(jpu_registration.id) AS total_client,
jpu_registration.idsc_center_id,
cc_disability_type.`name`,
jpu_registration.disability_type_id,
jpu_registration.registration_date
FROM
jpu_registration
INNER JOIN cc_disability_type ON cc_disability_type.id = jpu_registration.disability_type_id
GROUP BY
jpu_registration.disability_type_id
;
;
-- All Type - Seleted year

SELECT
COUNT(reg.id) AS total_client,
ccd.`name`,
reg.disability_type_id,
(SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,"%Y-%m") = DATE_FORMAT('2016-01-01','%Y-%m')) AS jan,
(SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,"%Y-%m") = DATE_FORMAT('2016-02-01','%Y-%m')) AS feb
FROM
jpu_registration AS reg
INNER JOIN cc_disability_type AS ccd ON ccd.id = reg.disability_type_id
 -- WHERE reg.disability_type_id = 1 and reg.idsc_center_id = 2
GROUP BY
reg.disability_type_id
ORDER BY ccd.weight DESC, ccd.`name` ASC 


----------------------------------------------------
//         public function __getReportInfo($id,$cc_table,$reg_cc_id,$year,$idscCenterId){
//
//             $sql = " SELECT ccTable.name AS type_name,ccTable.id AS type_id,idsc.id AS idsc_id,idsc.center_name,ccTable.name,
//                     reg.disability_type_id,COUNT(reg.id) AS total,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-01-01','%Y-%m')) AS jan_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-02-01','%Y-%m')) AS feb_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-03-01','%Y-%m')) AS mar_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-04-01','%Y-%m')) AS apr_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-05-01','%Y-%m')) AS may_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-06-01','%Y-%m')) AS jun_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-07-01','%Y-%m')) AS jul_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-08-01','%Y-%m')) AS aug_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-09-01','%Y-%m')) AS sep_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-10-01','%Y-%m')) AS oct_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-11-01','%Y-%m')) AS nov_count,
//                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-12-01','%Y-%m')) AS dec_count
//                    FROM
//                    jpu_registration AS reg
//                    INNER JOIN cc_disability_type AS ccTable ON ccTable.id = reg.disability_type_id
//                    INNER JOIN jpu_idsc_center AS idsc ON reg.idsc_center_id=idsc.id
////                    -- WHERE reg.disability_type_id = 1 and reg.idsc_center_id = 2
//                    ORDER BY ccTable.weight DESC, ccTable.name ASC
//                ";
//         if($year=='all'){
//            $sql = "SELECT ccTable.name AS type_name, ccTable.id AS type_id, idsc.id AS idsc_id,idsc.center_name, reg.idsc_center_id,
//                    COUNT(reg.id) AS total
//                    FROM
//                    jpu_registration AS reg
//                    INNER JOIN cc_disability_type AS ccTable ON ccTable.id = reg.disability_type_id
//                    INNER JOIN jpu_idsc_center AS idsc ON reg.idsc_center_id=idsc.id";
//        }
//        $sqlWhere = " ";
//
//        if ($id != 'all'){
//            $sql .= $sqlWhere . " Where reg.$reg_cc_id = '" . $id . "'";
//        }
//     if ($idscCenterId != 'all'){
//            $sql .= $sqlWhere . " AND reg.idsc_center_id = '" . $idscCenterId . "'";
//        }
//        $sql .= $sqlWhere . " GROUP BY reg.$reg_cc_id ";
//
//        $info = DB::select(DB::raw($sql));
//
//        return $info;
//
//    }
*************************************************
SELECT ccTable.name AS type_name,ccTable.id AS type_id,reg.idsc_center_id AS idsc_id,idsc.center_name, 
COUNT(*) AS total, 

SUM(CASE MONTH(reg.registration_date)
WHEN '01' THEN 1
ELSE 0 END) jan_count,

SUM(CASE MONTH(reg.registration_date)
WHEN '02' THEN 1
ELSE 0 END) feb_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '03' THEN 1
ELSE 0 END) mar_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '04' THEN 1
ELSE 0 END) apr_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '05' THEN 1
ELSE 0 END) may_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '06' THEN 1
ELSE 0 END) jun_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '07' THEN 1
ELSE 0 END) jul_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '08' THEN 1
ELSE 0 END) aug_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '09' THEN 1
ELSE 0 END) sep_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '10' THEN 1
ELSE 0 END) oct_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '11' THEN 1
ELSE 0 END) nov_count,
SUM(CASE MONTH(reg.registration_date)
WHEN '12' THEN 1
ELSE 0 END) dec_count

FROM jpu_registration reg 
INNER JOIN cc_disability_type AS ccTable ON reg.disability_type_id=ccTable.id 
INNER JOIN jpu_idsc_center AS idsc ON reg.idsc_center_id=idsc.id 
WHERE 1=1 AND
('' IS NULL OR YEAR(reg.registration_date)=2016) 
 AND reg.disability_type_id = 1
--  AND reg.idsc_center_id = '8' 
GROUP BY reg.disability_type_id 
*****************************************
SELECT ccTable.name AS type_name,ccTable.id AS type_id,idsc.id AS idsc_id,idsc.center_name,ccTable.name,
                    reg.disability_type_id,COUNT(reg.id) AS total,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-01-01','%Y-%m')) AS jan_count,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-02-01','%Y-%m')) AS feb_count,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-03-01','%Y-%m')) AS mar_count,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-04-01','%Y-%m')) AS apr_count,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-05-01','%Y-%m')) AS may_count,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-06-01','%Y-%m')) AS jun_count,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-07-01','%Y-%m')) AS jul_count,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-08-01','%Y-%m')) AS aug_count,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-09-01','%Y-%m')) AS sep_count,
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-10-01','%Y-%m')) AS oct_count,
		    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-11-01','%Y-%m')) AS nov_count,                    
                    (SELECT COUNT(r.id) FROM jpu_registration r WHERE r.disability_type_id = reg.disability_type_id AND reg.idsc_center_id=idsc.id AND DATE_FORMAT(r.registration_date,'%Y-%m') = DATE_FORMAT('2016-12-01','%Y-%m')) AS dec_count
                   FROM
                   jpu_registration AS reg
                   LEFT JOIN cc_disability_type AS ccTable ON ccTable.id = reg.disability_type_id
                   LEFT JOIN jpu_idsc_center AS idsc ON reg.idsc_center_id=idsc.id
                   WHERE reg.disability_type_id = 1 
                   ORDER BY ccTable.weight DESC, ccTable.name ASC
				   
				   ***************************************
				   
				   
				   SELECT ccTable.name AS type_name,reg.client_id, reg.client_name,  DATE_FORMAT(reg.registration_date,'%d-%m-%Y') AS registration_date, reg.date_of_birth,reg.gender,
                reg.father_name,reg.mother_name,reg.mobile,reg.husband_name,reg.village,
                thana.name AS thana,dis.name AS district,divi.name AS division
                FROM jpu_registration reg
                LEFT JOIN cc_disability_type AS ccTable ON reg.disability_type_id = ccTable.id
                LEFT JOIN thana_upazilas AS thana ON reg.thana_id=thana.id
                LEFT JOIN districts AS dis ON reg.district_id=dis.id
                LEFT JOIN divisions AS divi ON reg.division_id=divi.id
                WHERE 1=1 AND reg.disability_type_id = 1 AND MONTH(reg.registration_date) = '01'
                AND YEAR(reg.registration_date) = 2016
                AND reg.idsc_center_id = '8'
                
SELECT ccTable.name AS type_name,reg.client_id, reg.client_name, DATE_FORMAT(reg.registration_date,'%d-%m-%Y') AS registration_date, 
reg.date_of_birth,reg.gender, reg.father_name,reg.mother_name,reg.mobile,reg.husband_name,reg.village, thana.name AS thana,
dis.name AS district,divi.name AS division 
FROM jpu_registration reg 
LEFT JOIN cc_disability_type AS ccTable ON reg.disability_type_id = ccTable.id 
LEFT JOIN thana_upazilas AS thana ON reg.thana_id=thana.id 
LEFT JOIN districts AS dis ON reg.district_id=dis.id 
LEFT JOIN divisions AS divi ON reg.division_id=divi.id 
WHERE reg.disability_type_id = 1
*******************************************************